local env = std.extVar("__ksonnet/environments");
local params = std.extVar("__ksonnet/params").components["decompress-data-job"];

local k = import "k.libsonnet";

local decompressDataJob(namespace, name, pvc, dataPath, mountPath) = {
      apiVersion: "batch/v1",
      kind: "Job",
      metadata: {
        name: name,
        namespace: namespace,
      },
      spec: {
        template: {
          spec: {
            containers: [{
              name: "decompress-data",
              image: "ubuntu:16.04",
              imagePullPolicy: "IfNotPresent",
              command: ["tar", "--no-same-owner", "-xzvf",  dataPath, "-C", mountPath],
              volumeMounts: [{
                  mountPath: mountPath,
                  name: "model-data",
              },],
              },],
            volumes: [{
                name: "model-data",
                persistentVolumeClaim: {
                  claimName: pvc,
                },
            },],
            restartPolicy: "Never",
          },
        },
        backoffLimit: 4,
      },
    };

std.prune(k.core.v1.list.new([
  decompressDataJob(env.namespace, params.name + "-dataset", params.pvc, params.dataPath, params.mountPath)]))
