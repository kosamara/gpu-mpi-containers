local env = std.extVar("__ksonnet/environments");
local params = std.extVar("__ksonnet/params").components["get-repo-job"];

local k = import "k.libsonnet";

local getRepoJob(namespace, name, pvc, repoUrl, mountPath, repoName) = {
      apiVersion: "batch/v1",
      kind: "Job",
      metadata: {
        name: name,
        namespace: namespace,
      },
      spec: {
        template: {
          spec: {
            containers: [{
              name: "get-repos",
              image: "alpine/git",
              imagePullPolicy: "IfNotPresent",
              args: ["clone", repoUrl, mountPath+"/repo/"+repoName],
              volumeMounts: [{
                  mountPath: mountPath,
                  name: "model-data",
              },],
              }],
            volumes: [{
                name: "model-data",
                persistentVolumeClaim: {
                  claimName: pvc,
                },
            },],
            restartPolicy: "Never",
          },
        },
        backoffLimit: 4,
      },
    };

std.prune(k.core.v1.list.new([
  getRepoJob(env.namespace, params.name + "-model", params.pvc, params.urlModel, params.mountPath, params.modelName),
  getRepoJob(env.namespace, params.name + "-hptune", params.pvc, params.urlHPtune, params.mountPath, params.hptuneName)]))
