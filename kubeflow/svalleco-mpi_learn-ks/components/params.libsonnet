{
  global: {},
  components: {
    // Component-level parameters, defined initially from 'ks prototype use ...'
    // Each object below should correspond to a component in the components/ directory
    "model-pvc": {
      accessMode: 'ReadWriteMany',
      name: 'model-pvc1',
      storage: '46Gi',
    },
    "get-data-job": {
      dataPath: '/model/data_3dgan.tgz',
      mountPath: '/model',
      name: 'get-data-job',
      pvc: 'model-pvc1',
      urlData: 'https://cernbox.cern.ch/index.php/s/QVOlpkI9JC3XqHN/download?x-access-token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOiIyMDE4LTExLTA4VDEyOjQ2OjE5LjU2MjE3NTY4KzAxOjAwIiwiZXhwaXJlcyI6MCwiaWQiOiIxNDcxMjMiLCJpdGVtX3R5cGUiOjAsIm10aW1lIjoxNTQxNjA2NzM4LCJvd25lciI6InN2YWxsZWNvIiwicGF0aCI6ImVvc2hvbWUtczoxODE5NTYwMTU2OTA4NzQ4OCIsInByb3RlY3RlZCI6ZmFsc2UsInJlYWRfb25seSI6dHJ1ZSwic2hhcmVfbmFtZSI6ImRhdGFfM2RnYW4udGd6IiwidG9rZW4iOiJRVk9scGtJOUpDM1hxSE4ifQ.3H-Cq-kF1iPvRYY_Dik22LmpbRTIWleNAaYCCk_Xu1s',
    },
    "get-repo-job": {
      hptuneName: 'mpi_opt',
      modelName: 'mpi_learn',
      mountPath: '/model',
      name: 'get-repo-job',
      pvc: 'model-pvc1',
      urlHPtune: 'https://github.com/thongonary/mpi_opt.git',
      urlModel: 'https://github.com/svalleco/mpi_learn.git',
    },
    "decompress-data-job": {
      dataPath: '/model/data_3dgan.tgz',
      mountPath: '/model',
      name: 'decompress-data-job',
      pvc: 'model-pvc1',
    },
    "train-mpijob": {
      name: 'train-mpijob',
      replicas: 3,
      gpusPerReplica: 1,
      image: 'gitlab-registry.cern.ch/kosamara/gpu-mpi-containers/svalleco-mpirun',
      command: 'mpirun',
      args: '-x LD_LIBRARY_PATH,-x NV',
      mountPath: '/model',
      pvc: 'model-pvc1',
    },
  },
}
