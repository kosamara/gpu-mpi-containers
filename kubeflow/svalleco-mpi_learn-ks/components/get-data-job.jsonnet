local env = std.extVar("__ksonnet/environments");
local params = std.extVar("__ksonnet/params").components["get-data-job"];

local k = import "k.libsonnet";

local getDataJob(namespace, name, pvc, url, mountPath, dataPath) = {
      apiVersion: "batch/v1",
      kind: "Job",
      metadata: {
        name: name,
        namespace: namespace,
      },
      spec: {
        template: {
          spec: {
            containers: [{
              name: "get-decompress-data",
              image: "alpine",
              imagePullPolicy: "IfNotPresent",
              command: ["sh", "-c",
                "wget", "-O", dataPath, "--no-check-certificate", url, "&&",
                "tar", "--no-same-owner", "-xzvf",  dataPath, "-C", mountPath
              ],
              volumeMounts: [{
                  mountPath: mountPath,
                  name: "model-data",
              },],
              },],
            volumes: [{
                name: "model-data",
                persistentVolumeClaim: {
                  claimName: pvc,
                },
            },],
            restartPolicy: "Never",
          },
        },
        backoffLimit: 4,
      },
    };

std.prune(k.core.v1.list.new([
  getDataJob(env.namespace, params.name + "-dataset", params.pvc, params.urlData, params.mountPath, params.dataPath)]))
