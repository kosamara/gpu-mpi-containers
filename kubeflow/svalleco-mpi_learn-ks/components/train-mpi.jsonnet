local env = std.extVar("__ksonnet/environments");
local params = std.extVar("__ksonnet/params").components["train-mpijob"];

local k = import "k.libsonnet";

local namespace = env.namespace;  // namespace is inherited from the environment
local name = params.name;
local replicas = params.replicas;
local gpusPerReplica = params.gpusPerReplica;
local image = params.image;
local command = params.command;
local args = params.args;

local containerCommand =
  if command != "null" then
    {
      command: command,
    }
  else {};
local containerArgs =
  if args != "null" then
    {
      args: std.split(args, ","),
    }
  else {};

local mpiJobCustom = {
  apiVersion: "kubeflow.org/v1alpha1",
  kind: "MPIJob",
  metadata: {
    name: name,
    namespace: namespace,
  },
  spec: {
    replicas: replicas,
    template: {
      spec: {
        hostNetwork: "true",
        containers: [
          {
            name: name,
            image: image,
            resources: {
              limits: {
                "nvidia.com/gpu": gpusPerReplica,
              },
            },
            volumeMounts: [{
                mountPath: params.mountPath,
                name: "model-data",
            },],
          } + containerCommand + containerArgs,
        ],
        volumes: [{
            name: "model-data",
            persistentVolumeClaim: {
              claimName: params.pvc,
            },
        },],
      },
    },
  },
};

std.prune(k.core.v1.list.new([
  mpiJobCustom,
]))
